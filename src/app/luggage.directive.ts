import { Directive, ElementRef, Input } from '@angular/core';

@Directive({
  selector: '[appLuggage]'
})
export class LuggageDirective {

  @Input('appLuggage') luggage!: number;
  @Input('appLuggageClasseVol') classeVol!: string;



  constructor(private elementRef: ElementRef) {}

  ngOnInit() {
    this.highlightClass();
  }


  private highlightClass() {
    switch (this.classeVol) {
      case 'BUSINESS':
        if (this.luggage > 2)
        this.elementRef.nativeElement.style.color = 'red';
        else
        this.elementRef.nativeElement.style.color = 'black';
        break;
      case 'STANDARD':
        if (this.luggage > 1)
        this.elementRef.nativeElement.style.color = 'red';
        else
        this.elementRef.nativeElement.style.color = 'black';
        break;
      case 'PREMIUM':
        if (this.luggage > 3)
        this.elementRef.nativeElement.style.color = 'red';
        else
        this.elementRef.nativeElement.style.color = 'black';
        break;
      // Ajoutez d'autres cas pour chaque classe de vol
      default:
        this.elementRef.nativeElement.style.color = 'black'; // Couleur par défaut si la classe n'est pas gérée
        break;
    }
  }
}
