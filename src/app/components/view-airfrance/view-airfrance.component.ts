import { Component } from '@angular/core';
import { IFiltres } from 'src/app/models/filtres.model';
import { Vol } from 'src/app/models/vol.model';
import { VolService } from '../../services/vol.service';
import { PassagerService } from '../../services/passager.service';
import { ActivatedRoute } from '@angular/router';
import { Passager } from 'src/app/models/passager.model';

@Component({
  selector: 'app-view-airfrance',
  templateUrl: './view-airfrance.component.html',
  styleUrls: ['./view-airfrance.component.scss']
})
export class ViewAirFranceComponent {

  vols: Vol[] = [];
  vol!: Vol
  passagers!: Passager[];
  type!: string;



  ngOnInit(): void {
   let routeObserver = this._ActivatedRoute.data.subscribe(data$ => {
    this.type = data$['type'] ? data$['type'] :'decollages';
   });
  }



  constructor(
    private _volService: VolService,
    private _passagerService: PassagerService,
    private _ActivatedRoute: ActivatedRoute,
    ) { }
  

   passagersObserver = {
    next: (passagers: Passager[]) => this.passagers = passagers,
    error: (err: Error) => console.error('Observer got an error: ' + err),
    complete: () => console.log('Observer got a complete notification'),
  };

  setVol(vol: Vol) {
    this.vol = vol;
    this._passagerService.getPassagers(this.vol.icao).subscribe(this.passagersObserver);
    console.log(this.passagers);
  }
  /**
   * Réaction à la mise à jour des filtres
   * On souhaite récupérer les vols qui correspondent aux filtres passés en paramètre
   * en utilisant le service défini dans le constructeur
   * @param filtres récupérés depuis le composant enfant
   */
  
  volsObserver = {
    next: (vols: Vol[]) => this.vols = vols,
    error: (err: Error) => console.error('Observer got an error: ' + err),
    complete: () => console.log('Observer got a complete notification'),
  };

  

  onFiltresEvent(filtres: IFiltres): void {
    let type: string = "";
    console.log("type : " + this.type);
    switch (this.type) {
      case "decollages":
        type = "departure";
        break;
      case "atterrissages":
        type = "arrival";
        break;
    }
   // filtres.aeroport.icao, Math.floor(filtres.debut.getTime() / 1000), Math.floor(filtres.fin.getTime() / 1000)
   let filterObserver = this._volService.getVols(filtres.aeroport.icao, Math.floor(filtres.debut.getTime() / 1000), Math.floor(filtres.fin.getTime() / 1000), type).subscribe(this.volsObserver);
}
}