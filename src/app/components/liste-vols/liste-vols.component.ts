import { Component, Input, Output, EventEmitter } from '@angular/core';
import { IFiltres } from 'src/app/models/filtres.model';
import { Vol } from 'src/app/models/vol.model';

@Component({
  selector: 'app-liste-vols',
  templateUrl: './liste-vols.component.html',
  styleUrls: ['./liste-vols.component.scss']
})
export class ListeVolsComponent {
@Input() filtre!: IFiltres;
@Input() vols!: Vol[];
@Input() type!: string;
@Output() volEvent = new EventEmitter<Vol>();

selectVol(vol: Vol) {
  this.volEvent.emit(vol);
}
}
