import { Component, EventEmitter, Output, ViewEncapsulation } from '@angular/core';
import { MAT_DATE_RANGE_SELECTION_STRATEGY } from '@angular/material/datepicker';
import { FiveDayRangeSelectionStrategy } from 'src/app/date-adapter';
import { IAeroport } from 'src/app/models/aeroport.model';
import { AEROPORTS } from './../../constants/aeroport.constant';
import { IFiltres } from 'src/app/models/filtres.model';

@Component({
  selector: 'app-filtres',
  templateUrl: './filtres.component.html',
  styleUrls: ['./filtres.component.scss'],
  providers: [
    {
      provide: MAT_DATE_RANGE_SELECTION_STRATEGY,
      useClass: FiveDayRangeSelectionStrategy,
    },
  ],
  encapsulation: ViewEncapsulation.None
})
export class FiltresComponent {

  /**
   * La liste des aéroports disponibles est une constante,
   * on n'utilise que les principaux aéroports français pour l'instant
   */
  aeroports: IAeroport[] = AEROPORTS;
  @Output() filtresAppliques: EventEmitter<IFiltres> = new EventEmitter<IFiltres>();

  aeroportSelectionneValue!: IAeroport;
  dateDebutSelectionneeValue!: Date;
  dateFinSelectionneeValue!: Date;
  

  appliquerFiltres() {
    const aeroportSelectionne: IAeroport = this.aeroportSelectionneValue;
    const dateDebutSelectionnee: Date = this.dateDebutSelectionneeValue;
    const dateFinSelectionnee: Date = this.dateFinSelectionneeValue;
  
    const filtres: IFiltres = {
      aeroport: aeroportSelectionne,
      debut: dateDebutSelectionnee,
      fin: dateFinSelectionnee
    };
    
    this.filtresAppliques.emit(filtres);
  }
  


}
