import { Directive, ElementRef, Input, OnInit } from '@angular/core';

@Directive({
  selector: '[appClasseVol]'
})
export class ClasseVolDirective implements OnInit {
  @Input('appClasseVol') classeVol!: string;

  constructor(private elementRef: ElementRef) {}

  ngOnInit() {
    this.highlightClass();
  }

  private highlightClass() {
    switch (this.classeVol) {
      case 'BUSINESS':
        this.elementRef.nativeElement.style.color = 'red';
        break;
      case 'STANDARD':
        this.elementRef.nativeElement.style.color = 'blue';
        break;
      case 'PREMIUM':
        this.elementRef.nativeElement.style.color = 'green';
        break;
      // Ajoutez d'autres cas pour chaque classe de vol
      default:
        this.elementRef.nativeElement.style.color = 'black'; // Couleur par défaut si la classe n'est pas gérée
        break;
    }
  }
}
